# encoding=utf-8
import csv
import shutil
import os
from os.path import join, getsize


"""
It is better to use 'unicode' (type: str, the outer encoding is gbk) as the default 
encoding in the programs.

UTF-8 (type: byte) is also a choice to allow chinese symbols. Since
UTF-8 is different from unicode. It should first be decoded to unicode.

One can also directly use UTF-8. However the strings will be shown via codes
in Python.

Author: Zhangquan Zhou
Date: 2018-08
"""


def list_files(dir_path):
    """
    get all the files under **dir_path** as a list.
    """
    file_list = os.listdir(dir_path)
    return file_list


def recursively_list_files(root_dir):

    files = os.listdir(root_dir)
    real_files = []
    for file in files:
        file_path = os.path.join(root_dir, file)
        if os.path.isdir(file_path):
            sub_files = recursively_list_files(file_path)
            real_files.extend(sub_files)
        else:
            real_files.append(file_path)
    return real_files


def read_file_as_lines(file_path, encoding='utf-8'):
    """
    read files via the denoted encoding, which is utf-8 by default.
    """

    if encoding == 'unicode':
        f = open(file_path, 'r', errors='ignore')
    else:
        # if some illegal symbols occur, just ignore them.
        f = open(file_path, encoding=encoding, errors='ignore')

    # the above two ways both return string of type 'str'

    line_list = []
    for line in f.readlines():
        decoded_line = line.strip()
        line_list.append(decoded_line)
    f.close()

    return line_list


def read_file_as_text(file_path, encoding='utf-8'):
    """
    suppose that the target file is encoded via utf-8.
    """

    if encoding == 'unicode':
        f = open(file_path, 'r', errors='ignore')
    else:
        # if some illegal symbols occur, just ignore them.
        f = open(file_path, encoding=encoding, errors='ignore')

    # the above two ways both return string of type 'str'

    content = ""
    for line in f.readlines():
        decoded_line = line.strip()
        content += decoded_line + '\n'
    f.close()

    return content


def write_file_as_unicode(file_path, line_list, encoding='utf-8'):
    """
    write lines in **line_list** to **file_path**.
    the encoding is utf-8 by default.
    """

    f = open(file_path, 'w', encoding=encoding)
    for elem in line_list:
        # separated  by '\n'
        f.writelines(elem + '\n')
    f.close()


def move_file(src_file, dst_file):
    """
    move src_file to dst_file.
    """
    if not os.path.isfile(src_file):
        print("%s not exist!" % src_file)
    else:
        file_path, file_name = os.path.split(dst_file)      # 分离文件名和路径
        if not os.path.exists(file_path):
            os.makedirs(file_path)                          # 创建路径
        shutil.move(src_file, dst_file)                     # 移动文件
        # print("move %s -> %s" % (src_file, dst_file))


def copy_file(src_file, dst_file):
    """
    copy src_file to dst_file.
    """
    if not os.path.isfile(src_file):
        print("%s not exist!" % src_file)
    else:
        file_path, file_name = os.path.split(dst_file)     # 分离文件名和路径
        if not os.path.exists(file_path):
            os.makedirs(file_path)                         # 创建路径
        shutil.copyfile(src_file, dst_file)                # 复制文件
        # print("copy %s -> %s" % (src_file, dst_file))


def get_file_size(file_path):
    """
    return bit unit.
    K: /1024
    M: /1024/1024
    """
    return os.path.getsize(file_path)


def filter_files(src_dir, dest_dir, size):
    """
    将小于size的文件移动到dest_dir.
    size 以k为单位.
    """
    original_files = list_files(src_dir)
    _files = []

    for a_file in original_files:
        file_size = get_file_size(src_dir + a_file)
        if (file_size/1024) < size:
            """
            if the file is smaller than size(k),
            remove it out.
            """
            move_file(src_dir + a_file, dest_dir + a_file)
        else:
            _files.append(a_file)

    return _files


def read_terms_as_utf8(file_path):
    """
    读取术语文件，“#”行为解释行，“,”分隔
    """
    lines = read_file_as_utf8(file_path)
    term_list = []
    for line in lines:
        if line.startswith('#'):
            continue
        if line.startswith('\n'):
            continue
        if line.startswith('\r'):
            continue

        _term_list = line.split(',')
        term_list = term_list + _term_list
    return term_list


if __name__ == '__main__':
    pass
    # read_csv_as_unicode('../data1/corpus/core_corpus.csv', no_head=True)
    # file_list = list_files(r'D:/quanzz/AILab/裁判文书/snample/')
    #
    # i = 0
    # for file in file_list:
    #     print(i, file)
    #     i += 1

    # file_path = r'D:/quanzz/AILab/裁判文书/exceptions/eb5498a2-7735-4b39-8627-a80900336c7b.html'
    #
    # print(get_file_size(file_path)/1024)

