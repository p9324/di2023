# encoding = utf-8
import xlrd


class ExcelUtil(object):

    def __init__(self):
        self.excel_data = None

    def load(self, excel_path):
        self.excel_data = xlrd.open_workbook(excel_path)

    def read_data(self, sheet_index):
        # 返回一个列表，这个列表里的每一个元素是一个cell，cell有两个属性，一个ctype，一个value
        return self.excel_data.sheets()[sheet_index]

    def row_num(self, sheet_index):
        sheet = self.excel_data.sheets()[sheet_index]
        return sheet.nrows
