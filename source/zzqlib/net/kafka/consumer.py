# encoding = utf-8
from kafka import KafkaConsumer


class KafkaConsumeTool(object):
    """Kafka server"""

    def __init__(self, broker_address, topic, group_id, logger):
        self.logger = logger
        self.broker_address = broker_address
        self.broker_address_list = broker_address.split(',')
        self.topic = topic
        self.group_id = group_id
        self.kafka_consumer = None
        try:
            self.kafka_consumer = KafkaConsumer(topic, group_id=group_id, bootstrap_servers=self.broker_address_list)
            self.logger.info("a consumer connected to Kafka on address (%s), topic (%s), group id (%s)"
                             % (broker_address, topic, group_id))
        except Exception as e:
            self.logger.error(str(e))

        # 0: 初始，1: 接受， 2: 挂起；-1: 关闭
        self.status = 0

    def set_logger(self, logger):
        self.logger = logger

    def consume(self, processor, consume_limit=-1):
        """block mode to receive messages"""

        if self.kafka_consumer is not None:

            if self.status == 0:
                self.status = 1
                self.logger.info("consuming started")
            elif self.status == 1:
                pass
            elif self.status == 2:
                self.kafka_consumer.resume()
                self.status = 1
                self.logger.info("consuming continued")
            elif self.status == -1:
                self.logger.info("consuming closed")
                return

            consume_count = 0
            for msg in self.kafka_consumer:
                # process the message
                processor(msg)

                consume_count += 1

                if consume_count % 100 == 0:
                    self.logger.info("%d messages consumed" % consume_count)
                if consume_count >= 10000:
                    self.logger.info("another %d messages consumed" % consume_count)
                    consume_count = 0
                if consume_count >= consume_limit > 0:
                    self.logger.info(
                        "%d messages consumed, reached to the limit %d" % (consume_count, consume_limit))
                    self.kafka_consumer.pause()
                    self.logger.info("consuming paused")
                    self.status = 2
                    break

        else:
            self.logger.warn("the kafka consumer is not initialized")

    def close(self):
        if self.kafka_consumer is not None:
            self.kafka_consumer.close()
            self.status = -1
            self.logger.info("the kafka consumer is closed")
