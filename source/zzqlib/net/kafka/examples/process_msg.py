# encoding = utf-8


def process(msg):
    topic = msg.topic
    partition = msg.partition
    offset = msg.offset
    key = msg.key
    value = msg.value
    print("topic :%s, partition: %d, offset: %d, key: %s" % (topic, partition, offset, key))
    print(value.decode("utf-8"))


def parse_msg(msg):
    from sf_util import date_time
    data = msg["data"]
    return [
        data["entname"],
        data["title"],
        data["link"],
        date_time.timestamp2str(float(data["pubdate"])/1000),
        data["summary"],
        data["content"],
        data["website"],
        data["markedKeyword"]
    ]


if __name__ == '__main__':
    import json
    j = json.load(open('kafka_msg.json', 'r', encoding='utf-8'))
    print(parse_msg(j))