# encoding=utf-8


def retrieve_and_save(target_url, file_path, proxy_scheme=None, proxy_addr=None):
    import urllib.request

    if proxy_scheme and proxy_addr:
        # set proxy
        proxy_handler = urllib.request.ProxyHandler({proxy_scheme: proxy_addr})
        opener = urllib.request.build_opener(proxy_handler)
        # set the proxy as global
        urllib.request.install_opener(opener)

    urllib.request.urlretrieve(target_url, file_path)


def retrieve_html(html_url, encoding='utf-8', proxy_scheme=None, proxy_addr=None):
    import urllib.request

    if proxy_scheme and proxy_addr:
        # set proxy
        proxy_handler = urllib.request.ProxyHandler({proxy_scheme: proxy_addr})
        opener = urllib.request.build_opener(proxy_handler)
        # set the proxy as global
        urllib.request.install_opener(opener)

    page = urllib.request.urlopen(html_url)
    html_codes = page.read().decode(encoding)
    return html_codes


if __name__ == '__main__':
    html_content = retrieve_html('https://www.cnblogs.com/myyan/p/7149542.html',
                                 proxy_scheme='https', proxy_addr='http://10.37.235.10:8080')
    print(html_content)
