# encoding = utf-8
from zzqlib.net import retriever


def parse_url(url_str):
    """
    解析url.
    """
    from urllib.parse import urlparse
    parsed_result = urlparse(url_str)

    """
    ParseResult 继承于 namedtuple ，因此可以同时通过索引和命名属性来获取 URL 中各部分的值。
    print('scheme  :', parsed_result.scheme)
    print('netloc  :', parsed_result.netloc)
    print('path    :', parsed_result.path)
    print('params  :', parsed_result.params)
    print('query   :', parsed_result.query)
    print('fragment:', parsed_result.fragment)
    print('username:', parsed_result.username)
    print('password:', parsed_result.password)
    print('hostname:', parsed_result.hostname)
    print('port    :', parsed_result.port)
    """
    return parsed_result


def parse_html(html_codes, name='html'):
    """
    解析html.
    """
    from bs4 import BeautifulSoup
    bs = BeautifulSoup(html_codes, 'html.parser')
    name_list = bs.find_all(name=name)
    print(name_list[0])


if __name__ == '__main__':
    html_content = retriever.retrieve_html(
        'https://www.cnblogs.com/myyan/p/7149542.html',
        proxy_scheme='https', proxy_addr='http://10.37.235.10:8080')
    parse_html(html_content, name='h2')