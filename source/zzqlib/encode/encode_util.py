# encoding = utf-8


def int2bytes(i):
    return int.to_bytes(i, length=10, byteorder='big')


def bytes2int(bs):
    return int.from_bytes(bs, byteorder='big')


def str2bytes(strr, encoding='utf-8'):
    return bytes(strr, encoding)


def bytes2str(bs, encoding='utf-8'):
    return bs.decode(encoding)
