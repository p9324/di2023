# encoding=utf-8
"""
Author: Zhangquan Zhou
Date: 2016-07-27
Modified Dates: 2018-09-06
"""

import re


class REPattern(object):
    """
    The REPattern Interface.
    """

    def __init__(self, name=None, value=None, after_treat=None):
        self.name = name
        self.value = value
        self.after_treat = after_treat

        # this value should be denoted.
        self.re_expression = None

    def get_expression(self):

        return self.re_expression

    def match(self, text):
        """
        When a text is sent to this function, it is processed
        in two steps:
        1) match this text based on this REPattern object, if
        matched, return the matched part. If the **self.value** argument
        is not None, then cite the named part.
        2) the **self.after_treat** argument should be a recall function.
        If this argument is not None, this function should call
        the **self.after_treat** to further process the matched texts.
        It should be noted that **self.after_treat** returns a dict.
        """

        key = self.get_name()

        if self.after_treat:
            return self.after_treat(text, key, _match(self, text, value=self.value))
        else:
            return {key: _match(self, text, value=self.value)}

    def get_name(self):

        if not self.name:
            self.name = self.re_expression

        return self.name


class PlainPattern(REPattern):
    """
    PlainPattern:
    """
    def __init__(self, re_expression, name=None, value=None, after_treat=None):
        super(PlainPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = re_expression


class NamePattern(REPattern):
    """
    NamePattern: cite the given pattern by (?P<**cite_name**>),
    it is different from the name of the pattern.
    """

    def __init__(self, rp_pattern, cite_name, name=None, value=None, after_treat=None):
        super(NamePattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = r'(?P<' + cite_name + '>' + rp_pattern.get_expression() + ')'


class ANDPattern(REPattern):
    """
    ANDPattern: combine the given patterns in sequence.
    """

    def __init__(self, pattern_list, name=None, value=None, after_treat=None):
        super(ANDPattern, self).__init__(name=name, value=value, after_treat=after_treat)
        self.pattern_list = pattern_list
        self.re_expression = ""
        for pattern in self.pattern_list:
            if isinstance(pattern, REPattern):
                self.re_expression += pattern.get_expression()
            else:
                raise Exception("An object is not the REPattern instance.")


class SuffixPattern(REPattern):
    """
    SuffixPattern: add a suffix to the given pattern.
    This can also be implemented by ANDPattern.
    """

    def __init__(self, pre_expression, re_suffix, name=None, value=None, after_treat=None):
        super(SuffixPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = pre_expression.get_expression() \
                             + re_suffix.get_expression()


class PrefixPattern(REPattern):
    """
    PrefixPattern: add a prefix to the given pattern.
    This can also be implemented by ANDPattern.
    """

    def __init__(self, re_prefix, after_expression, name=None, value=None, after_treat=None):
        super(PrefixPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = re_prefix.get_expression()\
                             + after_expression.get_expression()


class MultiPattern(REPattern):
    """
    MultiPattern: occur more than 0 times.
    """

    def __init__(self, re_pattern, name=None, value=None, after_treat=None):
        super(MultiPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = '(' + re_pattern.get_expression() + ')+'


class OccurPattern(REPattern):
    """
    OccurPattern: occurred more than 0 times or didn't occur.
    """

    def __init__(self, re_pattern, name=None, value=None, after_treat=None):
        super(OccurPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = '(' + re_pattern.get_expression() + ')*'


class ExclusiveCharPattern(REPattern):
    """
    ExclusiveCharPattern: the given chars are exclusive.
    """

    def __init__(self, char_list, name=None, value=None, after_treat=None):
        super(ExclusiveCharPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        exclusive_chars = '[^'
        for char in char_list:
            exclusive_chars += char
        exclusive_chars += ']'
        self.re_expression = exclusive_chars


class OccurCharPattern(REPattern):
    """
    OccurCharPattern:
    This can also be realized by MultiORCharPattern with **num** set by -1.
    """

    def __init__(self, arg, name=None, value=None, after_treat=None):
        super(OccurCharPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        if type(arg) == list:
            re_or_chars = ""
            for char in arg:
                re_or_chars += char

            self.re_expression = r'[' + re_or_chars + r']*'

        elif isinstance(arg, ExclusiveCharPattern):
            self.re_expression = arg.get_expression() + r'*'

        else:
            raise Exception("The argument {} is illegal.".format(arg))


class MultiORCharPattern(REPattern):
    """
    MultiORCharPattern: multi-selected chars occurring more than 0 times.
    """

    def __init__(self, char_list, num, name=None, value=None, after_treat=None):
        super(MultiORCharPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        re_or_chars = ""
        for i in range(len(char_list)):
            if i == 0:
                re_or_chars = char_list[i]
            else:
                re_or_chars += "|" + char_list[i]

        if num == 0:
            # more than one
            self.re_expression = r'[' + re_or_chars + r']+'
        elif num > 0:
            self.re_expression = r'[' + re_or_chars + r']{' + str(num) + r'}'
        elif num < 0:
            # unlimited number
            self.re_expression = r'[' + re_or_chars + r']*'


class ORStringPattern(REPattern):
    """
    ORStringPattern: multi-selected strings occurring once.
    """

    def __init__(self, str_list, name=None, value=None, after_treat=None):
        super(ORStringPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        re_or_strs = ""
        for i in range(len(str_list)):
            if i == 0:
                re_or_strs = str_list[i]
            else:
                re_or_strs += "|" + str_list[i]

        self.re_expression = r'(' + re_or_strs + r')'


class ExclusiveStringPattern(REPattern):
    """
    ExclusivePattern: all the given strings are exclusive.
    """

    def __init__(self, str_list, name=None, value=None, after_treat=None):
        super(ExclusiveStringPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        ex_strs = ""
        for i in range(len(str_list)):
            if i == 0:
                ex_strs = str_list[i]
            else:
                ex_strs += "|" + str_list[i]

        self.re_expression = r'(?!.*(' + ex_strs + r')(.*))'


class HasPattern(REPattern):
    """
    HasPattern: the given pattern matches the texts surrounding by contexts **re_around**.
    """

    def __init__(self, re_around, has_expression, name=None, value=None, after_treat=None):
        super(HasPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = re_around.get_expression()\
                             + has_expression.get_expression() \
                             + re_around.get_expression()


class StartEndPattern(REPattern):
    """
    StartEndPattern:add '^' and '$' to the pattern.
    """

    def __init__(self, re_pattern, name=None, value=None, after_treat=None):
        super(StartEndPattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.re_expression = r'^' + re_pattern.get_expression() + r'$'


class InnerNameAllocator(object):
    """
    used for generating inner names.
    """

    def __init__(self):
        self.inner_count = 0

    def allocate(self):
        self.inner_count += 1
        return '_spid:' + str(self.inner_count)


inner_name_allocator = InnerNameAllocator()


class PipelinePattern(REPattern):
    """
    PipelinePattern: all the patterns are to be matched in sequence,
    if one fails, then next one, until successes.
    """

    def __init__(self, pattern_list, name=None, value=None, after_treat=None):
        super(PipelinePattern, self).__init__(name=name, value=value, after_treat=after_treat)

        self.pattern_list = pattern_list
        self.re_expression = self.name if self.name else inner_name_allocator.allocate()

    def match(self, text):
        """
        override the parent function.
        The **value** here should be a list that corresponds to
        the patterns in the **pattern_list**.
        The **after_treat** argument should also be a list.
        """

        self_name = self.get_name()

        for i in range(len(self.pattern_list)):
            re_pattern = self.pattern_list[i]
            match_dict = re_pattern.match(text)

            if len(match_dict) > 1:
                # this case is when after_treat works.
                # check whether there are some matches.
                for key in match_dict:
                    match = match_dict[key]
                    if len(match) > 0:
                        return match_dict
            else:
                # this case is when after_treat doesn't work.
                match = match_dict[re_pattern.get_name()]
                if len(match) > 0:
                    return {self_name: match}

        return {self_name: []}


# used for FiniteStatePattern.
STATE_HALT = 'state_halt'
SUCC_TO = 'matched_to'
FAIL_TO = 'unmatched_to'
SUCC_STEP = 'matched_step'
FAIL_STEP = 'unmatched_step'


class FiniteStatePattern(REPattern):
    """
    This class simulates the working procedure of FSM.
    """

    def __init__(self, pattern_list=None, transfer_mat=None, start_pattern=None,
                 name=None, value=None, after_treat=None):
        """
        Configure the finite state machine.
        :param pattern_list: a list of patterns, best with names.
        :param transfer_mat: a dict that denotes how to transfer from a pattern to another
               it looks like:
               {
                'state_1': {
                            'matched_to': 'next_state',
                            'unmatched_to': 'next_state',
                            'matched_step': 0,
                            'unmatched_step': 0
                          },
                'state_2': {
                            ...
                         },
                ...
               }
        :param start_pattern: the start pattern
        :param name: the name of this FSM.
        :param value: should be None.
        :param after_treat: should be None.
        """
        super(FiniteStatePattern, self).__init__(name=name, value=value, after_treat=after_treat)

        if not pattern_list or not transfer_mat or not start_pattern:
            raise Exception("The arguments are illegal.")

        self.re_expression = self.name if self.name else inner_name_allocator.allocate()

        # all the patterns
        self.pattern_dict = {}
        for pattern in pattern_list:
            self.pattern_dict[pattern.get_name()] = pattern

        # the translations, a dict
        self.transfer_mat = transfer_mat

        # start pattern name
        self.start_pattern = start_pattern

    def match(self, text_list):
        """
        The argument **text_list** should be a list.
        """

        if type(text_list) != list:
            raise Exception("The argument text should be a list")

        # used to save the match results.
        match_result = {}

        start_re_pattern = self.pattern_dict[self.start_pattern]
        current_text_index = 0
        current_pattern = start_re_pattern
        current_pattern_name = current_pattern.get_name()

        while current_text_index < len(text_list):
            current_text = text_list[current_text_index]

            # the match_dict can have multiple keys,
            # involving the current_pattern_name or other keys
            # that are allocated by the after_treat function.
            match_dict = current_pattern.match(current_text)

            # check whether there has some matches.
            has_match = False
            for key in match_dict:
                match = match_dict[key]
                if len(match) > 0:
                    has_match = True
                    if key in match_result:

                        # ignore the redundant ones
                        for match_text in match:
                            if match_text not in match_result[key]:
                                match_result[key].append(match_text)

                    else:
                        match_result[key] = match

            if has_match:
                # if something matched, transfer via the successful path.
                next_pattern_name = self.transfer_mat[current_pattern_name][SUCC_TO]
                matched_step = self.transfer_mat[current_pattern_name][SUCC_STEP]

                if next_pattern_name == STATE_HALT:
                    break
                else:
                    next_pattern = self.get_pattern(next_pattern_name)
                    current_text_index += matched_step
                    current_pattern = next_pattern
                    current_pattern_name = next_pattern_name
            else:
                # if nothing matched, transfer via the failure path.
                next_pattern_name = self.transfer_mat[current_pattern_name][FAIL_TO]
                unmatched_step = self.transfer_mat[current_pattern_name][FAIL_STEP]

                if next_pattern_name == STATE_HALT:
                    break
                else:
                    next_pattern = self.get_pattern(next_pattern_name)
                    current_text_index += unmatched_step
                    current_pattern = next_pattern
                    current_pattern_name = next_pattern_name

        return match_result

    def get_pattern(self, pattern_name):

        return self.pattern_dict[pattern_name]


def _match(re_pattern, text, value=None):
    """
    This function uses the **re** module.
    The matching procedure is based on **finditer**,
    since it returns a iterator of matched objects.
    On the other hand, **findall** returns a list of string,
    which cannot call **group** .
    """

    re_expression = re_pattern.get_expression()
    pat_obj = re.compile(re_expression, re.X)
    match_obj_iter = pat_obj.finditer(text)

    matched_list = []
    for match in match_obj_iter:
        if value:
            match_text = match.group(value)
            if match_text not in matched_list:
                matched_list.append(match_text)
        else:
            match_text = match.group()
            if match_text not in matched_list:
                matched_list.append(match_text)

    # it could be empty
    return matched_list



