# encoding=utf-8


class WordGraph(object):
    """
    a set of words.
    """

    def __init__(self):
        self.word_list = None
        self.HEAD = '<HEAD>'

        # the word dict
        self.word_dict = {}
        head = Word(self.HEAD)
        self.word_dict[self.HEAD] = head

        # the syntax
        self.syntax_list = []

        # the semantic
        self.semantic_list = []

    def add_word(self, token):

        if token not in self.word_dict:
            new_word = Word(token)
            self.word_dict[token] = new_word

    def load_words(self, word_list):

        self.word_list = word_list

        for word in word_list:
            self.add_word(word)

    def add_postag(self, word, postag):

        if word in self.word_dict:
            self.word_dict[word].add_postag(postag)
        else:
            raise Exception("no word named %s in the graph.", word)

    def add_netag(self, word, netag):

        if word in self.word_dict:
            self.word_dict[word].add_netag(netag)
        else:
            raise Exception("no word named %s in the graph.", word)

    def add_arc(self, start, rel, end):

        if start in self.word_dict and end in self.word_dict:
            start_word = self.word_dict[start]
            end_word = self.word_dict[end]
            start_word.add_start_arc(rel, end)
            end_word.add_end_arc(rel, start)
            self.syntax_list.append("%s(%s,%s)" % (rel, start, end))

        elif start not in self.word_dict:
            raise Exception("no word named %s in the graph.", start)

        elif end not in self.word_dict:
            raise Exception("no word named %s in the graph.", end)

    def add_role(self, predicate, role_label, word_group):
        role = ','.join(word_group)
        self.semantic_list.append("%s(%s(%s))" % (predicate, role_label, role))

    def wordsAsString(self):
        desc = ''
        for token in self.word_dict:
            if token != self.HEAD:
                word = self.word_dict[token]
                desc += word.desc() + ' '
        return desc

    def syntaxAsString(self):
        desc = ''
        for syntax in self.syntax_list:
            desc += syntax + ' '
        return desc

    def rolesAsString(self):
        desc = ''
        for role in self.semantic_list:
            desc += role + ' '
        return desc


class Word(object):
    """
    word.
    """

    def __init__(self, token):
        self.token = token
        self.postag = ''
        self.netag = ''
        self.role_label = ''
        self.start_arc = ('', '')
        self.end_arc = ('', '')

    def add_postag(self, postag):
        self.postag = postag

    def add_netag(self, netag):
        self.netag = netag

    def add_role(self, role):
        self.role_label = role

    def add_start_arc(self, rel, end):

        self.start_arc = (rel, end)

    def add_end_arc(self, rel, start):

        self.end_arc = (rel, start)

    def desc(self):
        return "%s(%s,%s)" % (self.token, self.postag, self.netag)
