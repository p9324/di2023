# encoding=utf-8

from zzqlib.nlp.ltp_util import LTPUtil
from zzqlib.nlp.word_graph import WordGraph


def word_graph_via_ltp(sentence, ltputil, default=True):

    words = list(ltputil.segment(sentence, default=default))

    word_graph = WordGraph()
    word_graph.load_words(words)

    postags = ltputil.postag(words)
    netags = ltputil.recognize(words, postags)
    arcs = ltputil.parse(words, postags)

    for i in range(len(words)):
        word = words[i]
        postag = postags[i]
        netag = netags[i]
        arc = arcs[i]
        word_graph.add_postag(word, postag)
        word_graph.add_netag(word, netag)

        if arc.head == 0:
            word_graph.add_arc('<HEAD>', arc.relation, word)
        else:
            word_graph.add_arc(words[arc.head - 1], arc.relation, word)

    roles = ltputil.label(words, postags, arcs)
    for role in roles:
        predicate = words[role.index]
        for arg in role.arguments:
            role_type = arg.name
            word_group = words[arg.range.start:(arg.range.end+1)]
            word_graph.add_role(predicate, role_type, word_group)

    return word_graph


if __name__ == '__main__':
    MODEL_DIR = r'D:/quanzz/AILab/learning/ltp/ltp_data_v3.4.0/ltp_data_v3.4.0/'

    ltputil = LTPUtil()

    ltputil.load_models(
        seg_model_path=MODEL_DIR + 'cws.model',
        pos_model_path=MODEL_DIR + 'pos.model',
        ner_model_path=MODEL_DIR + 'ner.model',
        parser_model_path=MODEL_DIR + 'parser.model',
        srl_model_path=MODEL_DIR + 'pisrl_win.model')
    ltputil.load_dict(
        segmentor_model_path=MODEL_DIR + 'cws.model',
        dict_path=r'data1/mydict')

    sentence = '亚硝酸盐是一种化学物质'

    word_graph = word_graph_via_ltp(sentence, ltputil, default=False)
    ltputil.release_models()

    print("===========words==========")
    print(word_graph.wordsAsString())

    print("===========syntax==========")
    print(word_graph.syntaxAsString())

    print("===========semantic==========")
    print(word_graph.rolesAsString())

