# encoding=utf-8
import numpy as np
from scipy import sparse


def build_crs_matrix(sparse_list, vector_len):

    indptr = [0]
    indices = []
    data = []

    current_count = 0
    for row in sparse_list:
        current_count += len(row)
        indptr.append(current_count)
        for (pos, ele) in row:
            indices.append(pos)
            data.append(ele)

    indptr = np.array(indptr)
    indices = np.array(indices)
    data = np.array(data)

    crs_matrix = sparse.csr_matrix((data, indices, indptr), shape=(len(sparse_list), vector_len))
    return crs_matrix


# l = [[(0, 23), (3, 16), (8, 5), (20, 454)],
#      [(1, 21), (10, 56)]]
# crs_mat = build_crs_matrix(l, 21)
# print(crs_mat)







