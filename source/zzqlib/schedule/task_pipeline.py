# encoding = utf-8
import traceback


class TaskPipeline(object):

    def __init__(self):
        self.logger = None
        self.task_pipeline = []
        self.task_pipeline_conf = None

    def set_logger(self, logger):
        self.logger = logger
        for subtask in self.task_pipeline:
            subtask.set_logger(logger)

    def set_pipeline(self, pipeline_conf):
        self.task_pipeline_conf = pipeline_conf

    def boot(self):
        for subtask_handler in self.task_pipeline_conf:
            subtask = subtask_handler()
            subtask.set_logger(self.logger)
            subtask.boot()
            self.task_pipeline.append(subtask)
        self.logger.info("task pipeline booting finished")

    def run_pipeline(self, param):

        try:
            input_param = param
            for subtask in self.task_pipeline:
                output_result = subtask.process(input_param)
                if "_status" in output_result:
                    if not output_result["_status"]:
                        self.logger.debug("some error occurs, the task pipeline halts")
                        return
                input_param = output_result
            self.logger.debug("all sub-tasks in the task pipeline successfully completed")

        except Exception:
            self.logger.error("some error occurs, see the following: ")
            self.logger.error(traceback.format_exc())
