# coding = utf-8
import datetime
import time
from sf_util import date_time
from sf_util import log_util


def sleep(hour, minute, sec):
    sleep_time = hour*3600 + minute*60 + sec
    time.sleep(sleep_time)


class TaskScheduler(object):

    def __init__(self, task, logger):
        self.logger = logger
        self.task = task
        self.current_date = None

    def run(self, time_point, freq='d'):
        while True:
            if self._time_on(time_point, freq=freq):
                # refresh logger
                self.logger = self.task.get_logger()
                self.current_date = date_time.date2str()
                start = datetime.datetime.now()
                self.logger.info(
                    "running task \"%s\" at %s %d:%d:%d" % (
                        self.task.desc(), self.current_date, start.hour, start.minute, start.second))
                self.task.run()
                end = datetime.datetime.now()
                self.logger.info(
                    "task finished at %d:%d:%d" % (end.hour, end.minute, end.second))
            date_time.print_time()

    @staticmethod
    def _time_on(time_point, freq='d'):
        now = datetime.datetime.now()
        if freq == 'd':
            if time_point['h'] == now.hour and time_point['m'] == now.minute and time_point['s'] == now.second:
                return True
        elif freq == 'h':
            if time_point['m'] == now.minute and time_point['s'] == now.second:
                return True
        elif freq == 'm':
            if time_point['s'] == now.second:
                return True
        return False


# class TestTask(object):
#
#     def desc(self):
#         return "test task"
#
#     def run(self):
#         print("i am doing something")
#
#
# if __name__ == '__main__':
#
#     import logging
#     logger = log_util.get_logger("test", logging.DEBUG)
#
#     test_task = TestTask()
#     task_scheduler = TaskScheduler(test_task, logger)
#     time_conf = {
#         'h': 0,
#         'm': 0,
#         's': 12
#     }
#     task_scheduler.run(time_point=time_conf, freq='h')
