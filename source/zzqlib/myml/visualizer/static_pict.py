# encoding = utf-8
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import mpl_finance as mpf
import numpy as np


def draw_curve(x, y):
    plt.plot(x, y, '-')
    plt.show()


def draw_k_data(ax, k_data):
    """
    should be ['open', 'high', 'low', 'close']
    :param k_data:
    :return:
    """

    num_of_dates = len(k_data)
    candle_data = np.column_stack([list(range(num_of_dates)), k_data])
    mpf.candlestick_ohlc(ax, candle_data, width=0.2, colorup='r', colordown='g')
    return ax


class StaticDrawer(object):

    def __init__(self):
        self.fig = plt.figure(figsize=(10, 6))
        self.ax = self.fig.add_axes([0.1, 0.1, 0.8, 0.8])

    def draw_k(self, k_data):
        draw_k_data(self.ax, k_data)

    def draw_opts(self, k_data, opt_signals):

        fields = ['open', 'high', 'low', 'close']
        high_offset = fields.index('high')
        low_offset = fields.index('low')

        sell_offset = []
        sell_dots = []
        buy_offset = []
        buy_dots = []
        for i in range(len(opt_signals)):
            signal = opt_signals[i]
            opt_data = k_data[i]

            if signal == 0:
                buy_dots.append(opt_data[low_offset])
                buy_offset.append(i)
            elif signal == 2:
                sell_dots.append(opt_data[high_offset])
                sell_offset.append(i)

        self.ax.plot(buy_offset, buy_dots, '^', color='red', ms=4)
        self.ax.plot(sell_offset, sell_dots, 'v', color='g', ms=4)

    @staticmethod
    def show():
        plt.show()
