# encoding = utf-8
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Dropout


class KerasFNNWrapper(object):

    def __init__(self, logger):
        self.logger = logger
        self.model = None

    def load(self, model_path):
        self.model = load_model(model_path)
        self.logger.info("[MyFNN] model from the file %s loaded" % model_path)

    def save(self, model_path):
        self.model.save(model_path)
        self.logger.info("[MyFNN] model saved in the file %s" % model_path)

    def initialize(self, model_setting):

        nn = []
        for layer in model_setting['layers']:

            neurons = layer['neurons'] if 'neurons' in layer else None
            dropout_rate = layer['dropout_rate'] if 'dropout_rate' in layer else None
            activation = layer['activation'] if 'activation' in layer else None
            input_shape = layer['input_shape'] if 'input_shape' in layer else None

            if layer['type'] == 'dense':
                if input_shape:
                    nn.append(Dense(neurons, input_shape=input_shape))
                else:
                    nn.append(Dense(neurons))
                nn.append(Activation(activation))
            elif layer['type'] == 'dropout':
                nn.append(Dropout(dropout_rate))

        self.model = Sequential(nn)
        self.model.compile(
            loss=model_setting['loss'],
            optimizer=model_setting['optimizer'],
            metrics=model_setting['metrics']
        )

        self.logger.info("[MyFNN] fnn constructed")

    def train(self, x, y, train_setting):

        self.logger.info("[MyFNN] %s training start %s" % ("=" * 10, "=" * 10))
        history = self.model.fit(x, y, epochs=train_setting['epochs'],
                                 batch_size=train_setting['batch_size'],
                                 verbose=train_setting['verbose'])
        self.logger.info("[MyFNN] %s training done %s" % ("=" * 10, "=" * 10))
        return history.history

    def predict(self, x, batch_size=None):
        return self.model.predict(x=x, batch_size=batch_size)

    def predict_classes(self, x, batch_size=None):
        return self.model.predict_classes(x=x, verbose=0, batch_size=batch_size)

    def evaluate(self, features, reals, batch_size=None):
        return self.model.evaluate(x=features, y=reals, verbose=0, batch_size=batch_size)