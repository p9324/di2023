# encoding = utf-8
from keras.layers import Dense, Dropout, LSTM
from keras.models import Sequential, load_model


class KerasLSTM(object):

    def __init__(self, logger):
        self.logger = logger
        self.model = None

    def load(self, model_path):
        """
        从给定路径加载模型
        :return:
        """
        self.model = load_model(model_path)
        self.logger.info("[MyLSTM] model from the file %s loaded" % model_path)

    def save(self, model_path):
        self.model.save(model_path)
        self.logger.info("[MyLSTM] model saved in the file %s" % model_path)

    def initialize(self, model_setting):

        self.model = Sequential()
        for layer in model_setting['layers']:

            neurons = layer['neurons'] if 'neurons' in layer else None
            dropout_rate = layer['dropout_rate'] if 'dropout_rate' in layer else None
            activation = layer['activation'] if 'activation' in layer else None
            return_seq = layer['return_seq'] if 'return_seq' in layer else None
            input_timesteps = layer['input_timesteps'] if 'input_timesteps' in layer else None
            input_dim = layer['input_dim'] if 'input_dim' in layer else None

            if layer['type'] == 'dense':
                self.model.add(Dense(neurons, activation=activation))
            if layer['type'] == 'lstm':
                # return_sequences=True 返回整个序列，False仅返回最后一个值.
                self.model.add(LSTM(neurons, input_shape=(input_timesteps, input_dim), return_sequences=return_seq))
            if layer['type'] == 'dropout':
                self.model.add(Dropout(dropout_rate))

        self.model.compile(
            loss=model_setting['loss'],
            optimizer=model_setting['optimizer'],
            metrics=model_setting['metrics']
        )

        self.logger.info("[MyLSTM] Model initialized")

    def train(self, x, y, train_setting):

        self.logger.info("[MyLSTM] %s training start %s" % ("="*10, "="*10))
        history = self.model.fit(x, y, epochs=train_setting['epochs'],
                                 batch_size=train_setting['batch_size'],
                                 verbose=train_setting['verbose'])
        self.logger.info("[MyLSTM] %s training done %s" % ("=" * 10, "=" * 10))
        return history.history
