# encoding = utf-8
from zzqlib.quant.zen.zenner import Zenner
from zzqlib.quant import data_set
from zzqlib.quant.zen.eval import generating
from zzqlib.common import process_bar
from zzqlib.quant.zen.draw import pict_tool


class ZenManager(object):

    def __init__(self, logger):
        self.logger = logger
        self.dataset = None
        self.zenner = Zenner(logger)
        self.zen_data = {
            "_slices": [],
            "_status": False
        }
        self.outputer = generating.ResultGenerator(logger)
        self.start_date = None
        self.end_date = None

    def load_data(self, data_path, field_offset_dict=None, used_fields=None):

        if not field_offset_dict:
            field_offset_dict = {
                "date": 0,
                "open": 1,
                "high": 2,
                "low": 3,
                "close": 4
            }

        if not used_fields:
            used_fields = ["open", "high", "low", "close"]

        self.dataset = data_set.DataSet()
        self.dataset.load_data(
            data_path,
            offset_dict=field_offset_dict,
            used_fields=used_fields
        )

        self.logger.info("[ZenManager] data loaded from %s" % data_path)

    """
    BACKTESTING
    """

    def backtest(self, backtest_setting, start_date=None, end_date=None, print_process=False):
        """
        :param backtest_setting:
        :param start_date:
        :param end_date:
        :param print_process:
        :return:
        """

        self.zenner.boot(backtest_setting["pipeline"])

        if not start_date:
            start_date = self.dataset.get_date(0)

        if not end_date:
            end_date = self.dataset.get_date(-1)

        self.start_date = start_date
        self.end_date = end_date

        k_dates = self.dataset.get_dates(start_date=start_date, end_date=end_date)
        k_data = self.dataset.get_batch_data(start_date=start_date, end_date=end_date)

        total_count = len(k_data)
        opt_history = []

        self.logger.info("[ZenManager] %s backtesting start %s" % ("=" * 10, "=" * 10))
        for i in range(0, total_count):

            # build slice
            data_slice = {
                "_status": False,
                "k_data": k_data[0: i + 1],
                "k_dates": k_dates[0: i + 1],
                "_args": {
                    "interval": backtest_setting["interval"],
                    "min_range": backtest_setting["min_range"]
                }
            }

            # run pipeline
            data_slice = self.zenner.process(data_slice)

            # record history
            opt_history.append({
                "opt_type": data_slice["opt_type"],
                "opt_offset": data_slice["opt_offset"],
                "opt_desc": data_slice["opt_desc"],
            })

            data_slice["opt_history"] = opt_history.copy()
            self.zen_data["_slices"].append(data_slice)

            if print_process:
                process_bar.process_bar(i, total_count)

        if print_process:
            process_bar.process_bar(total_count, total_count)
            print("\n")

        self.zen_data["_status"] = True
        self.logger.info("[ZenManager] backtesting done")

    def last_available_opt(self):

        if not self.zen_data["_status"]:
            self.logger.error("[ZenManager] backtesting undone")
            return

        data_slice = self.zen_data["_slices"][-1]
        opt_history = data_slice["opt_history"]

        for i in range(len(opt_history)-1, -1, -1):
            opt_dict = opt_history[i]
            opt_type = opt_dict["opt_type"]
            if opt_type != 0:
                return opt_type

    """
    PROCESS RESULTS
    """

    def generate_slice(self, pict_path, data_slice_index=-1, show_adj_k=False, show_line=True, show_pivot=True, show_opt=True):
        """
        :param data_slice_index:
        :param pict_path:
        :param show_adj_k:
        :param show_line:
        :param show_pivot:
        :param show_opt:
        :return:
        """

        if not self.zen_data["_status"]:
            self.logger.error("[ZenManager] backtesting undone")
            return

        data_slice_list = self.zen_data["_slices"]
        data_slice = data_slice_list[data_slice_index]
        pict_tool.save(data_slice, pict_path, show_adj_k=show_adj_k, show_line=show_line, show_pivot=show_pivot, show_opt=show_opt)
        self.logger.info("[ZenManager] slice drawn and saved in %s" % pict_path)

    def generate_slices(self, html_path):
        """
        :param html_path:
        :return:
        """

        if not self.zen_data["_status"]:
            self.logger.error("[ZenManager] backtesting undone")
            return

        data_slice_list = self.zen_data["_slices"]
        fig, ax = pict_tool.new_ax()
        pict_tool.draw_dynamic_zen(fig, ax, data_slice_list, html_path=html_path)

    def generate_opts(self, signal_path):

        if not self.zen_data["_status"]:
            self.logger.error("[ZenManager] backtesting undone")
            return

        data_slice = self.zen_data["_slices"][-1]
        k_data = data_slice["k_data"]
        k_dates = data_slice["k_dates"]
        opt_history = data_slice["opt_history"]
        opt_type = []

        for opt_dict in opt_history:
            opt_type.append(opt_dict["opt_type"])

        self.outputer.generate_opts(k_data, k_dates, signal_path, opt_type)
        self.logger.info("[ZenManager] all signals generated and dumped to %s" % signal_path)

