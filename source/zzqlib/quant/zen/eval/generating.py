# encoding = utf-8
from zzqlib.io import csv_util


class ResultGenerator(object):

    def __init__(self, logger):
        self.logger = logger

    def generate_opts(self, k_data, k_dates, signal_path, opt_type):
        """
        generate signals
        :param k_data:
        :param k_dates:
        :param signal_path:
        :param opt_type:
        :return:
        """

        rows = []
        for i in range(0, len(k_data)):
            rows.append([k_dates[i], k_data[i][3], opt_type[i]])
        csv_util.dump_csv(signal_path, ['date', 'close price', 'signals'], rows)
