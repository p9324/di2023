# coding = utf-8
from zzqlib.common import process_bar
from zzqlib.io import csv_util
from zzqlib.quant.zen.eval import nets_pict


class Evaluator(object):

    def __init__(self):
        pass

    @staticmethod
    def eval(signal_path, encoding='utf-8', stop_loss=-1, allow_short=True):
        """do evaluation.

         Args:
            signal_path (str): the path of signals
            encoding (str): encoding
            stop_loss (float): grater than 0, -1 denotes no stop loss
            allow_short
        """
        signal_data = csv_util.read_csv(signal_path, has_head=True, encoding=encoding)
        date_labels = []
        bench_closes = []
        stgy_nets = []
        bench_nets = []
        stgy_opts = []

        if not signal_data:
            raise Exception("no signal data loaded")

        print("[Evaluator] do evaluating.")
        is_init = True
        count_process = 0
        count_stop_loss = 0
        for (date, bench_close, stgy_opt) in signal_data:
            bench_close = float(bench_close)
            stgy_opt = int(stgy_opt)

            if is_init:
                bench_nets.append(1)
                stgy_nets.append(1)
                stgy_opts.append(stgy_opt)
                is_init = False
            else:
                bench_change = (bench_close - bench_closes[-1])/bench_closes[-1]
                bench_net = bench_nets[-1] + bench_nets[-1]*bench_change

                if allow_short:
                    stgy_change = bench_change * stgy_opts[-1]
                else:
                    if stgy_opts[-1] == -1:
                        stgy_change = 0
                    else:
                        stgy_change = bench_change

                if stop_loss > 0:
                    if stgy_change < 0 and abs(stgy_change) > abs(stop_loss):
                        stgy_change = 0 - abs(stop_loss)
                        count_stop_loss += 1

                stgy_net = stgy_nets[-1] + stgy_nets[-1]*stgy_change
                stgy_opt = stgy_opt if stgy_opt != 0 else stgy_opts[-1]
                bench_nets.append(bench_net)
                stgy_nets.append(stgy_net)
                stgy_opts.append(stgy_opt)

            date_labels.append(date)
            bench_closes.append(bench_close)
            count_process += 1
            process_bar.process_bar(count_process, len(signal_data))

        print("\n[Evaluator] evaluating done, stop loss percent %.2f%%." % (count_stop_loss*100 / len(signal_data)))
        nets_pict.draw_nets(stgy_nets, bench_nets, date_labels)


if __name__ == '__main__':
    evaluator = Evaluator()
    evaluator.eval(r'../ext/signal/signal_hs300_d_2008-1-17.csv', stop_loss=-1)
