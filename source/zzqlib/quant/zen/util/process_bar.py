# coding = utf-8
import sys
import math


def process_bar(current, total):
    """打印进度条"""
    percent = '{:.2%}'.format(float(current) / float(total))
    sys.stdout.write('\r')
    sys.stdout.write("[%-50s] %s" % ("=" * int(math.floor(current * 50 / total)), percent))
    sys.stdout.flush()
