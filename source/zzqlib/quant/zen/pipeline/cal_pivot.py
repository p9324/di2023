# encoding = utf-8


open_offset = 0
high_offset = 1
low_offset = 2
close_offset = 3


class CalPivot(object):

    def __init__(self, logger):
        self.logger = logger

    """
    Input:
    {
        "_status": ...
        "_args": {"interval": 1, "min_range": 0.02}
        "k_data": ...
        "k_dates": ...
        "k_adj_data": ...
        "trend": ...
        "fractal_type": [...]     # 分型的类型
        "fractal_offset": [...]   # 分型的偏移
    }

    Output:
    {
        "_status": ...
        "_args": {"interval": 1, "min_range": 0.02}
        "k_data": ...
        "k_dates": ...
        "k_adj_data": ...
        "trend": ...
        "fractal_type": [...]     # 分型的类型
        "fractal_offset": [...]   # 分型的偏移
        "pivots": [...]           # 中枢列表（从前往后）
    }

    shape(#numOfPivots), each value is a dict that is as follows:
        {
            "left":     0,  # left offset of tickers
            "right":    0,  # right offset of tickers
            "low":      0,  # low price of pivot
            "high":     0,  # high price of pivot
            "top":      0,  # top price of pivot
            "bottom":   0,  # bottom price of pivot
            "fractals": []  # fractals' offsets of tickers
        }
    """
    def process(self, data):

        if "k_adj_data" not in data:
            self.logger.error("[CalPivot] \"k_adj_data\" not in data")
            data["_status"] = False
            return data

        if "fractal_type" not in data:
            self.logger.error("[CalPivot] \"fractal_type\" not in data")
            data["_status"] = False
            return data

        if "fractal_offset" not in data:
            self.logger.error("[CalPivot] \"fractal_offset\" not in data")
            data["_status"] = False
            return data

        if "_args" not in data:
            self.logger.error("[CalPivot] \"_args\" not in data")
            data["_status"] = False
            return data

        if "min_range" not in data["_args"]:
            self.logger.error("[CalPivot] \"min_range\" not in data[\"_args\"]")
            data["_status"] = False
            return data

        k_adj_data = data["k_adj_data"]
        min_range = data["_args"]["min_range"]

        i = len(k_adj_data) - 1
        if i <= 0:
            # 如果只有1根k线就不计算了
            data["pivots"] = []
            data["_status"] = True
            return data
        else:
            pivots = []
            right_bound = -1
            # 从倒数第一根k往前算
            while i > 0:
                # 从i这个位置往前计算中枢
                current_pivot = self._compute_pivot(data, current_offset=i, right_bound=right_bound, min_range=min_range)
                if current_pivot is not None:
                    pivots.insert(0, current_pivot)
                    last_observe_offset = current_pivot["last"]
                    right_bound = current_pivot["left"]
                    if last_observe_offset > 0:
                        i = last_observe_offset
                    else:
                        i = 0
                elif current_pivot is None:
                    # 没有完整的一笔，说明已经到头了，结束计算
                    break

            data["pivots"] = pivots
            data["_status"] = True
            return data

    def _cal_mean(self, value_list):
        mean_value = float(0)
        for v in value_list:
            mean_value += v
        mean_value = mean_value / len(value_list)
        return mean_value

    def _compute_pivot(self, data, current_offset=-1, right_bound=-1, min_range=0.02):
        """
        计算中枢
        注意：一个中枢至少有三笔
        current_offset: 观测点
        observe_offset: 观测分型
        返回中枢的边界
        """

        k_adj_data = data["k_adj_data"]
        fractal_type = data["fractal_type"]
        fractal_offset = data["fractal_offset"]

        if current_offset == -1:
            # 默认情况计算最后一根k线所处中枢
            current_offset = len(k_adj_data) - 1

        """
        寻找最近的观测分型点：中枢需要从起始分型点开始计算
        但是观测点到这个分型也应该算（不完整的）一笔
        """

        observe_offset = -1
        observe_index = -1
        for i in range(len(fractal_offset) - 1, -1, -1):
            offset = fractal_offset[i]
            if offset <= current_offset \
                    and i != len(fractal_offset) - 1 and i != 0:
                # 观测分型点位小于观测k线点位
                # 观测分型点不是最后一根k
                # 观测分型点不是第一根k
                observe_offset = offset  # 分型的偏移
                observe_index = i        # 分型的编号
                break

        if observe_offset == -1 or observe_index <= 0:
            # 从观测分型点往前，没有完整的一笔，不进行中枢的计算
            return None

        """
        计算中枢的右边界
        """

        if right_bound == -1:
            right_bound = current_offset

        # 初始化中枢
        current_pivot = {
            "last": 0,              # 上一个观测点
            "left": 0,              # 左边界offset
            "right": right_bound,   # 右边界offset
            "low": 0,               # 严格下界
            "high": 0,              # 严格上界
            "top": [0],             # 松弛上界（总值，分母=顶分型数），最后的值=总值/分母
            "bottom": [0],          # 松弛下界（总值，分母=底分型数），最后的值=总值/分母
            "fractals": []          # 所包含的分型偏移list
        }

        for i in range(observe_index, -1, -1):
            """
            从观测分型开始往前遍历分型找中枢
            两个分型就可以定中枢
            """

            current_fractal_offset = fractal_offset[i]            # 当前分型偏移
            current_fractal_type = fractal_type[i]                # 当前分型类型
            current_fractal_num = len(current_pivot["fractals"])  # 目前的分型数

            if current_fractal_type == 1:
                """当前分型为顶分型"""

                # 取出当前分型最高点
                current_fractal_dot = k_adj_data[current_fractal_offset][high_offset]

                if current_fractal_num == 0:
                    # 目前中枢没有分型，就以当前顶分型作为上界
                    current_pivot["high"] = current_fractal_dot
                    current_pivot["top"] = [current_fractal_dot]
                    current_pivot["fractals"].insert(0, current_fractal_offset)

                elif current_fractal_num == 1:
                    # 目前中枢有1个分型，一定是底分型，仍以当前顶分型作为上界
                    current_pivot["high"] = current_fractal_dot
                    current_pivot["top"] = [current_fractal_dot]
                    current_pivot["fractals"].insert(0, current_fractal_offset)

                elif current_fractal_num >= 2:

                    # 向上分离
                    mean_top = self._cal_mean(current_pivot["top"])
                    mean_bottom = self._cal_mean(current_pivot["bottom"])
                    if (current_fractal_dot - mean_top) >= (mean_top - mean_bottom):
                        current_pivot["left"] = (current_pivot["fractals"][0] + current_fractal_offset) / 2.0
                        current_pivot["last"] = current_fractal_offset
                        break

                    # 向下分离
                    if current_pivot["low"] >= current_fractal_dot:
                        fake_offset = current_pivot["fractals"].pop(0)
                        current_pivot["left"] = (current_pivot["fractals"][0] + fake_offset) / 2.0
                        current_pivot["last"] = fake_offset
                        current_pivot["bottom"].pop()
                        break

                    # 不满足分离
                    if current_pivot["high"] >= current_fractal_dot:
                        current_pivot["high"] = current_fractal_dot
                    current_pivot["top"].append(current_fractal_dot)
                    current_pivot["left"] = current_fractal_offset
                    current_pivot["fractals"].insert(0, current_fractal_offset)

            elif current_fractal_type == -1:
                """当前分型为底分型"""

                current_fractal_dot = k_adj_data[current_fractal_offset][low_offset]

                if current_fractal_num == 0:
                    # 目前中枢没有分型，就以当前底分型作为下界
                    current_pivot["low"] = current_fractal_dot
                    current_pivot["bottom"] = [current_fractal_dot]
                    current_pivot["fractals"].insert(0, current_fractal_offset)

                elif current_fractal_num == 1:
                    # 目前中枢有1个分型，仍以当前顶分型作为下界
                    current_pivot["low"] = current_fractal_dot
                    current_pivot["bottom"] = [current_fractal_dot]
                    current_pivot["fractals"].insert(0, current_fractal_offset)

                elif current_fractal_num >= 2:

                    # 向下分离
                    mean_top = self._cal_mean(current_pivot["top"])
                    mean_bottom = self._cal_mean(current_pivot["bottom"])
                    if (mean_bottom - current_fractal_dot) >= (mean_top - mean_bottom):
                        current_pivot["left"] = (current_pivot["fractals"][0] + current_fractal_offset) / 2.0
                        current_pivot["last"] = current_fractal_offset
                        break

                    # 向上分离
                    if current_pivot["high"] <= current_fractal_dot:
                        fake_offset = current_pivot["fractals"].pop(0)
                        current_pivot["left"] = (current_pivot["fractals"][0] + fake_offset) / 2.0
                        current_pivot["last"] = fake_offset
                        current_pivot["top"].pop()
                        break

                    # 不满足分离条件
                    if current_pivot["low"] <= current_fractal_dot:
                        current_pivot["low"] = current_fractal_dot
                    current_pivot["bottom"].append(current_fractal_dot)
                    current_pivot["left"] = current_fractal_offset
                    current_pivot["fractals"].insert(0, current_fractal_offset)

            elif current_fractal_type == 0:
                """遇到第一根k线"""
                current_pivot["left"] = (current_pivot["fractals"][0] + 0 ) / 2.0
                current_pivot["last"] = 0
                break

        """
        计算松弛的上下界
        """
        current_pivot["top"] = self._cal_mean(current_pivot["top"])
        if current_pivot["top"] == 0:
            current_pivot["top"] = current_pivot["high"]

        current_pivot["bottom"] = self._cal_mean(current_pivot["bottom"])
        if current_pivot["bottom"] == 0:
            current_pivot["bottom"] = current_pivot["low"]

        if current_pivot["high"] == 0 or current_pivot["low"] == 0:
            """没有结果，没有三笔的情况，不计算中枢"""
            return None
        else:

            if (current_pivot["high"] - current_pivot["low"]) / current_pivot["low"] < min_range:
                """
                重新进行中枢严格上下界调整
                """
                middle_dot = (current_pivot["high"] + current_pivot["low"]) / 2.0
                current_pivot["high"] = middle_dot + (middle_dot*(min_range/2))
                current_pivot["low"] = middle_dot - (middle_dot*(min_range/2))

            if current_pivot["top"] < current_pivot["high"]:
                # 调整松弛上界
                current_pivot["top"] = current_pivot["high"]
            if current_pivot["bottom"] > current_pivot["low"]:
                # 调整松弛下界
                current_pivot["bottom"] = current_pivot["low"]

            return current_pivot

    # def _compute_pivot(self, data, current_offset=-1, min_range=0.02):
    #     """
    #     计算中枢
    #     注意：一个中枢至少有三笔
    #     current_offset: 观测点
    #     observe_offset: 观测分型
    #     返回中枢的边界
    #     """
    #
    #     k_adj_data = data["k_adj_data"]
    #     fractal_type = data["fractal_type"]
    #     fractal_offset = data["fractal_offset"]
    #
    #     if current_offset == -1:
    #         # 默认情况计算最后一根k线所处中枢
    #         current_offset = len(k_adj_data) - 1
    #
    #     """
    #     寻找最近的观测分型点：中枢需要从起始分型点开始计算
    #     """
    #
    #     observe_offset = -1
    #     observe_index = -1
    #     for i in range(len(fractal_offset) - 1, -1, -1):
    #         offset = fractal_offset[i]
    #         if offset <= current_offset \
    #                 and i != len(fractal_offset) - 1 and i != 0:
    #             # 观测分型点位小于观测k线点位
    #             # 观测分型点不是最后一根k
    #             # 观测分型点不是第一根k
    #             observe_offset = offset  # 分型的偏移
    #             observe_index = i        # 分型的编号
    #             break
    #
    #     if observe_offset == -1 or observe_index <= 0:
    #         # 从观测分型点往前，没有完整的一笔，不进行中枢的计算
    #         return None
    #
    #     """
    #     计算中枢的右边界
    #     """
    #
    #     if observe_index < len(fractal_offset) - 1:
    #         # 这里observe_index + 1也包含了最后一根k线
    #         # 观测分型与下一个分型的中位作为中枢右边界
    #         right_bound = (observe_offset + fractal_offset[observe_index + 1]) / 2.0
    #     else:
    #         # 这种情况其实不可能的，因为 i != len(self.fractal_offset) - 1
    #         right_bound = observe_offset
    #
    #     # 初始化中枢
    #     current_pivot = {
    #         "left": 0,              # 左边界offset
    #         "right": right_bound,   # 右边界offset
    #         "low": 0,               # 严格下界
    #         "high": 0,              # 严格上界
    #         "top": [0],             # 松弛上界（总值，分母=顶分型数），最后的值=总值/分母
    #         "bottom": [0],          # 松弛下界（总值，分母=底分型数），最后的值=总值/分母
    #         "fractals": []          # 所包含的分型偏移list
    #     }
    #
    #     for i in range(observe_index, -1, -1):
    #         """从观测分型开始往前遍历分型找中枢"""
    #
    #         current_fractal_offset = fractal_offset[i]            # 当前分型偏移
    #         current_fractal_type = fractal_type[i]                # 当前分型类型
    #         current_fractal_num = len(current_pivot["fractals"])  # 目前的分型数
    #
    #         if current_fractal_type == 1:
    #             """当前分型为顶分型"""
    #
    #             # 取出当前分型最高点
    #             current_fractal_dot = k_adj_data[current_fractal_offset][high_offset]
    #
    #             if current_fractal_num == 0:
    #                 # 目前中枢没有分型，就以当前顶分型作为上界
    #                 current_pivot["high"] = current_fractal_dot
    #                 current_pivot["top"] = [current_fractal_dot]
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #             elif current_fractal_num == 1:
    #                 # 目前中枢有1个分型，一定是底分型，仍以当前顶分型作为上界
    #                 current_pivot["high"] = current_fractal_dot
    #                 current_pivot["top"] = [current_fractal_dot]
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #             elif current_fractal_num == 2:
    #                 # 目前中枢有2个分型，有一个顶分型，因此要重新计算上界
    #                 if current_pivot["high"] >= current_fractal_dot:
    #                     current_pivot["high"] = current_fractal_dot
    #                 # if current_pivot["top"] < current_fractal_dot:
    #                 #     current_pivot["top"] = current_fractal_dot
    #                 current_pivot["top"].append(current_fractal_dot)
    #                 current_pivot["left"] = current_fractal_offset
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #             elif current_fractal_num >= 3:
    #                 if current_pivot["low"] >= current_fractal_dot:
    #                     fake_offset = current_pivot["fractals"].pop(0)
    #                     current_pivot["left"] = (fake_offset + current_pivot["fractals"][0]) / 2.0
    #                     current_pivot["bottom"].pop()
    #                     break
    #                 else:
    #                     if current_pivot["high"] >= current_fractal_dot:
    #                         current_pivot["high"] = current_fractal_dot
    #                     # if current_fractal_num == 3 and current_pivot["top"] < current_fractal_dot:
    #                     #     current_pivot["top"] = current_fractal_dot
    #                     current_pivot["top"].append(current_fractal_dot)
    #                     current_pivot["left"] = current_fractal_offset
    #                     current_pivot["fractals"].insert(0, current_fractal_offset)
    #
    #         elif current_fractal_type == -1:
    #             """当前分型为底分型"""
    #
    #             current_fractal_dot = k_adj_data[current_fractal_offset][low_offset]
    #
    #             if current_fractal_num == 0:
    #                 # 目前中枢没有分型，就以当前底分型作为下界
    #                 current_pivot["low"] = current_fractal_dot
    #                 current_pivot["bottom"] = [current_fractal_dot]
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #
    #             elif current_fractal_num == 1:
    #                 # 目前中枢有1个分型，仍以当前顶分型作为下界
    #                 current_pivot["low"] = current_fractal_dot
    #                 current_pivot["bottom"] = [current_fractal_dot]
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #
    #             elif current_fractal_num == 2:
    #                 # 目前中枢有2个分型，重新计算下界
    #                 if current_pivot["low"] <= current_fractal_dot:
    #                     current_pivot["low"] = current_fractal_dot
    #                 # if current_pivot["bottom"] > current_fractal_dot:
    #                 #     current_pivot["bottom"] = current_fractal_dot
    #                 current_pivot["bottom"].append(current_fractal_dot)
    #                 current_pivot["left"] = current_fractal_offset
    #                 current_pivot["fractals"].insert(0, current_fractal_offset)
    #
    #             elif current_fractal_num >= 3:
    #                 # 目前中枢有3个以上分型
    #                 if current_pivot["high"] <= current_fractal_dot:
    #                     fake_offset = current_pivot["fractals"].pop(0)
    #                     current_pivot["left"] = (fake_offset + current_pivot["fractals"][0]) / 2.0
    #                     current_pivot["top"].pop()
    #                     break
    #
    #                 else:
    #                     if current_pivot["low"] <= current_fractal_dot:
    #                         current_pivot["low"] = current_fractal_dot
    #                     # if current_fractal_num == 3 and current_pivot["bottom"] > current_fractal_dot:
    #                     #     current_pivot["bottom"] = current_fractal_dot
    #                     current_pivot["bottom"].append(current_fractal_dot)
    #                     current_pivot["left"] = current_fractal_offset
    #                     current_pivot["fractals"].insert(0, current_fractal_offset)
    #
    #     """
    #     计算松弛的上下界
    #     """
    #     relax_top = float(0)
    #     for t in current_pivot["top"]:
    #         relax_top += t
    #     current_pivot["top"] = relax_top / len(current_pivot["top"])
    #     if current_pivot["top"] == 0:
    #         current_pivot["top"] = current_pivot["high"]
    #
    #     relax_bottom = float(0)
    #     for b in current_pivot["bottom"]:
    #         relax_bottom += b
    #     current_pivot["bottom"] = relax_bottom / len(current_pivot["bottom"])
    #     if current_pivot["bottom"] == 0:
    #         current_pivot["bottom"] = current_pivot["low"]
    #
    #     if current_pivot["high"] == 0 or current_pivot["low"] == 0:
    #         """没有结果，没有三笔的情况，不计算中枢"""
    #         return None
    #     else:
    #
    #         if (current_pivot["high"] - current_pivot["low"]) / current_pivot["low"] < min_range:
    #             """
    #             重新进行中枢严格上下界调整
    #             """
    #             middle_dot = (current_pivot["high"] + current_pivot["low"]) / 2.0
    #             current_pivot["high"] = middle_dot + (middle_dot*(min_range/2))
    #             current_pivot["low"] = middle_dot - (middle_dot*(min_range/2))
    #
    #         if current_pivot["top"] < current_pivot["high"]:
    #             # 调整松弛上界
    #             current_pivot["top"] = current_pivot["high"]
    #         if current_pivot["bottom"] > current_pivot["low"]:
    #             # 调整松弛下界
    #             current_pivot["bottom"] = current_pivot["low"]
    #
    #         return current_pivot
