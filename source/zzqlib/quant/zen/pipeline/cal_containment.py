# encoding = utf-8


"""
左包右：根据收盘价调整趋势
"""


# 默认都是“开、高、低、收”
open_offset = 0
high_offset = 1
low_offset = 2
close_offset = 3


class CalContainment(object):

    def __init__(self, logger):
        self.logger = logger

    def process(self, data):
        """
        trend, 0:没有趋势；1:向上趋势；-1:向下趋势
        """

        if "k_data" not in data:
            self.logger.error("[CalContainment] the field \"k_data\" is not in data")
            data["_status"] = False
            return data

        k_data = data["k_data"]
        trend = [0]
        k_adj_data = [k_data[0].copy()]
        for i in range(1, len(k_data)):
            # 向前调整，根据前面的k线进行调整

            previous_trend = trend[i - 1]
            current_k = k_data[i].copy()
            previous_adj_k = k_adj_data[i - 1]

            # 两根k线相全等
            if previous_adj_k[high_offset] == current_k[high_offset] \
                    and previous_adj_k[low_offset] == current_k[low_offset]:
                # 趋势延续
                trend.append(previous_trend)

            # 向右包含 I > i
            elif previous_adj_k[high_offset] >= current_k[high_offset] \
                    and previous_adj_k[low_offset] <= current_k[low_offset]:
                # 趋势延续
                trend.append(previous_trend)
                if previous_trend == -1:
                    current_k[low_offset] = previous_adj_k[low_offset]
                elif previous_trend == 1:
                    current_k[high_offset] = previous_adj_k[high_offset]

            # 向左包含 i < I
            elif previous_adj_k[high_offset] <= current_k[high_offset] \
                    and previous_adj_k[low_offset] >= current_k[low_offset]:

                # 根据I和i的收盘价调整
                if current_k[close_offset] > previous_adj_k[close_offset]:
                    current_trend = 1
                elif current_k[close_offset] < previous_adj_k[close_offset]:
                    current_trend = -1
                else:
                    current_trend = previous_trend

                trend.append(current_trend)

                if current_trend == -1:
                    current_k[high_offset] = previous_adj_k[high_offset]
                elif current_trend == 1:
                    current_k[low_offset] = previous_adj_k[low_offset]

            # 不是全包含情况：下行
            elif previous_adj_k[high_offset] > current_k[high_offset] \
                    and previous_adj_k[low_offset] > current_k[low_offset]:
                trend.append(-1)

            # 不是全包含情况：上行
            elif previous_adj_k[high_offset] < current_k[high_offset] \
                    and previous_adj_k[low_offset] < current_k[low_offset]:
                trend.append(1)

            # 根据更新的high和low调整开盘价和收盘价
            if current_k[open_offset] > current_k[close_offset]:
                if current_k[open_offset] > current_k[high_offset]:
                    current_k[open_offset] = current_k[high_offset]
                if current_k[close_offset] < current_k[low_offset]:
                    current_k[close_offset] = current_k[low_offset]
            else:
                if current_k[open_offset] < current_k[low_offset]:
                    current_k[open_offset] = current_k[low_offset]
                if current_k[close_offset] > current_k[high_offset]:
                    current_k[close_offset] = current_k[high_offset]

            k_adj_data.append(current_k)

        data["k_adj_data"] = k_adj_data
        data["trend"] = trend
        data["_status"] = True
        return data
