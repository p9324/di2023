import torch
from torch_geometric.nn import MessagePassing
from torch_scatter import scatter
from torch.nn import Linear, Parameter


class SimpleEdgeGNN(MessagePassing):
    r"""
    .. math::
        \mathbf{e}_i^{(t+1)}=\mathbf{e}_i^{(t)}+\sum_{\mathbf{e}_k\in\mathcal{HO}(\mathbf{e}_i)}{\mathbf{W}_k\mathbf{e}_k^{(t)}}
        +\mathbf{b}

    """

    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 edge_attr='sum'):
        super().__init__()

        self.edge_aggr = edge_attr

        self.linear = Linear(in_channels, out_channels, bias=False)
        self.bias = Parameter(torch.Tensor(out_channels))

        self.relu = torch.nn.ReLU()

        self.reset_parameters()

    def reset_parameters(self):
        self.linear.reset_parameters()
        self.bias.data.zero_()

    def forward(self, e, edge_index, ho_index, edge_num):
        # self.propagate(edge_index, x=x)  # this is for node updating
        adj_e = self.edge_updater(edge_index, ho_index=ho_index, edge_num=edge_num, e=e)
        adj_e = self.linear(adj_e) + self.bias
        new_e = adj_e + e

        new_e = self.relu(new_e)

        return new_e

    def edge_update(self, edge_index, ho_index, edge_num, e):
        # edge_index is useless

        # select
        source_index = ho_index[1]
        target_index = ho_index[0]
        # collect all high-order neighborhoods
        collect_ = e.index_select(dim=0, index=source_index)

        # aggregate
        aggr_e = scatter(collect_, target_index, dim=0, dim_size=edge_num, reduce=self.edge_aggr)

        return aggr_e
