import torch
from torch_geometric.nn.conv.message_passing import MessagePassing
import torch_geometric.nn as geom_nn
from torch_scatter import scatter
from torch.nn import Linear, Parameter


class NPRGNNModel(MessagePassing):
    r"""
    .. math::
        \mathbf{e}_i^{(t+1)}=\mathbf{H}\mathbf{e}_i^{(t)}+(\sum_{\mathbf{e}_k\in\mathcal{HO}(\mathbf{e}_i)}{\mathbf{W}_k\mathbf{e}_k^{(t)}}+\mathbf{b})+\mathbf{d}

        \mathbf{x}_i^{(t+1)}=\mathbf{H'}\mathbf{x}_i^{(t)}+(\sum_{\mathbf{x}_k\in\mathcal{E}(\mathbf{x}_i)}{\mathbf{W'}_k\mathbf{x}_k^{(t)}}+\mathbf{b'})+\mathbf{d'}

        \mathbf{e_out}_i^{(t+1)}=\mathbf{W_1}\mathbf{e}_i^{(t+1)} + \mathbf{W_1}\mathbf{x}_i^{(t+1)} + \mathbf{b}_1

        \mathbf{x_out}_i^{(t+1)}=\mathbf{W_2}\mathbf{x}_i^{(t+1)} + \mathbf{W_2}\mathbf{e}_i^{(t+1)} + \mathbf{b}_2
    """

    def __init__(self,

                 node_in_channels: int,
                 node_output_channels: int,

                 edge_in_channels: int,
                 edge_output_channels: int,

                 node_aggr='sum',
                 edge_aggr='sum'

                 ):
        super().__init__()

        self.node_aggr = node_aggr
        self.edge_aggr = edge_aggr

        # -- build edge layers --

        self.linear_edge_neighbor = Linear(edge_in_channels, edge_in_channels, bias=True)
        self.linear_edge_self = Linear(edge_in_channels, edge_in_channels, bias=False)
        self.linear_edge_out = Linear(edge_in_channels, edge_output_channels, bias=True)

        # -- build node layers --

        self.linear_node_neighbor = Linear(node_in_channels, node_in_channels, bias=True)
        self.linear_node_self = Linear(node_in_channels, node_in_channels, bias=False)
        self.linear_node_out = Linear(node_in_channels, node_output_channels, bias=True)

        # -- activation functions --

        self.relu = torch.nn.ReLU()
        self.sigmoid = torch.nn.Sigmoid()

        # -- readout -----
        self.readout = geom_nn.global_add_pool

        self.reset_parameters()

    def reset_parameters(self):
        for layer in [self.linear_edge_neighbor, self.linear_edge_self, self.linear_edge_out,
                      self.linear_node_neighbor, self.linear_node_self, self.linear_node_out,]:
            layer.reset_parameters()

    def forward(self, x, e, edge_index, ho_index, node_num, edge_num, batch=None):
        """
        :param x: [node_num, dim_num]
        :param e: [edge_num, dim_num]
        :param edge_index:
        :param ho_index:
        :param node_num:
        :param edge_num:
        :param batch:
        :return: out [num,]
        """
        # -- propagating edge information

        # hidden layer
        adj_e = self.edge_updater(edge_index, ho_index=ho_index, edge_num=edge_num, e=e)
        adj_e = self.linear_edge_neighbor(adj_e)
        new_e = adj_e + self.linear_edge_self(e)
        # new_e = self.relu(new_e)

        # -- propagating node information

        # hidden layer
        adj_x = self.propagate(edge_index, node_num=node_num, x=x)  # this is for node propagating
        adj_x = self.linear_node_neighbor(adj_x)
        new_x = adj_x + self.linear_node_self(x)
        # new_e = self.relu(new_e)

        # -- output layer

        read_e = self.readout(new_e, None)
        read_e = read_e.repeat(len(new_e), 1)
        read_x = self.readout(new_x, None)
        read_x = read_x.repeat(len(new_x), 1)

        new_e = new_e + read_e
        new_x = new_x + read_x

        e_out = self.sigmoid(self.linear_edge_out(new_e))
        x_out = self.sigmoid(self.linear_node_out(new_x))

        return x_out, e_out

    def propagate(self, edge_index, node_num, x):

        # select
        source_index = edge_index[1]
        target_index = edge_index[0]
        collect_ = x.index_select(dim=0, index=source_index)

        # aggregate
        aggr_x = scatter(collect_, target_index, dim=0, dim_size=node_num, reduce=self.node_aggr)

        return aggr_x

    def edge_update(self, edge_index, ho_index, edge_num, e):
        # edge_index is useless

        # select
        source_index = ho_index[1]
        target_index = ho_index[0]
        # collect all high-order neighborhoods
        collect_ = e.index_select(dim=0, index=source_index)

        # aggregate
        aggr_e = scatter(collect_, target_index, dim=0, dim_size=edge_num, reduce=self.edge_aggr)

        return aggr_e
