import torch
from torch_geometric.nn.conv import GINConv
from torch_geometric.nn.conv.message_passing import MessagePassing


class GINModel(MessagePassing):

    def __init__(self,
                 in_channels: int,
                 out_channels: int):
        super().__init__()

        self.middle_level = torch.nn.Sequential(
            torch.nn.Linear(in_channels, out_channels),
            torch.nn.Sigmoid()
        )

        self.gin_model = GINConv(self.middle_level, eps=0, train_eps=False)

        self.reset_parameters()

    def reset_parameters(self):
        self.gin_model.reset_parameters()

    def forward(self, x, e, edge_index, ho_index, node_num, edge_num, batch=None):
        """

        :param e: [num, dim_num]
        :param ho_index:
        :param batch:
        :return: out [num,]
        """

        out = self.gin_model.forward(x=e, edge_index=ho_index)

        return None, out


class GINNodeModel(MessagePassing):

    def __init__(self,
                 in_channels: int,
                 out_channels: int):
        super().__init__()

        self.middle_level = torch.nn.Sequential(
            torch.nn.Linear(in_channels, out_channels),
            torch.nn.Sigmoid()
        )

        self.gin_model = GINConv(self.middle_level, eps=0, train_eps=False)

        self.reset_parameters()

    def reset_parameters(self):
        self.gin_model.reset_parameters()

    def forward(self, x, e, edge_index, ho_index, node_num, edge_num, batch=None):
        """

        :param e: [num, dim_num]
        :param ho_index:
        :param batch:
        :return: out [num,]
        """

        out = self.gin_model.forward(x=x, edge_index=edge_index)

        return out, None
