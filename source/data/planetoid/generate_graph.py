# encoding=utf-8
import torch
from models.utils import compute_ho_index
from data.utils import C2GraphData
from torch_geometric.data import Data
from zzqlib.io import pickle_util
from zzqlib.common import process_bar
from zzqlib.io import csv_util

# ---------------------------
# make C2Graph
# ---------------------------


def generate_commontype_C2graph(data_name):
    commontype_path = r"../../datasets/planetoid/preprocessed/%s.commontype.csv" % data_name
    commontype_rows = csv_util.read_csv(commontype_path, has_head=True)

    citation_graph = C2GraphData()

    for [cited, citation, ptype] in commontype_rows:
        citation_graph.add_node_with_labels(cited, [ptype])
        citation_graph.add_node_with_labels(citation, [ptype])
        citation_graph.add_edge(citation, cited)

    return citation_graph


def generate_transcitations_C2graph(data_name):

    transcite_path = r"../../datasets/planetoid/preprocessed/%s.transcite.csv" % data_name
    transcite_rows = csv_util.read_csv(transcite_path, has_head=True)

    transcite_graph = C2GraphData()
    for [x, x_type, y, y_type, z, z_type] in transcite_rows:
        for [paper, paper_type] in [[x, x_type], [y, y_type], [z, z_type]]:
            transcite_graph.add_node_with_labels(paper, [paper_type])

        transcite_graph.add_edge_with_labels(y, x, ["0-level"])
        transcite_graph.add_edge_with_labels(z, y, ["0-level"])
        transcite_graph.add_edge_with_labels(z, x, ["1-level"])

    return transcite_graph


# ---------------------------
# make PyG graph
# ---------------------------


def generate_commontype_graph(data_name):
    """generate commontype graph"""

    graph = generate_commontype_C2graph(data_name)
    paper_num = graph.node_num()
    type_num = graph.node_label_num()
    citation_num = graph.edge_num()

    # node features
    node_features = [[0 for _ in range(type_num)] for _ in range(paper_num)]
    x_citations = graph.nodes_outgoing()
    for x_citation in x_citations:
        x_type = graph.get_node_label_by_nid(x_citation)
        node_features[x_citation][x_type] = 1
    x = torch.tensor(node_features, dtype=torch.float)

    # node labels
    y_labels = [[0 for _ in range(type_num)] for _ in range(paper_num)]
    for _y in graph.get_node_ids():
        y_type = graph.get_node_label_by_nid(_y)
        y_labels[_y][y_type] = 1
    y = torch.tensor(y_labels, dtype=torch.float)

    # edge features
    edge_features = [[1] for _ in range(citation_num)]

    # edge index
    edge_i = []
    edge_j = []
    for edge_id in range(graph.edge_num()):
        (i, j) = graph.get_edge_by_id(edge_id)
        edge_i.append(i)
        edge_j.append(j)
    edge_index = torch.tensor([edge_i, edge_j], dtype=torch.long)

    # edge attr
    edge_attr = torch.tensor(edge_features, dtype=torch.float)

    # edge labels
    edge_labels = torch.tensor(edge_features, dtype=torch.float)

    # ho index
    ho_index = graph.compute_ho_index()
    ho_index = torch.tensor(ho_index, dtype=torch.long)

    # train, val, test mask
    train_mask = torch.tensor([True for _ in range(paper_num)], dtype=torch.bool)
    val_mask = train_mask
    test_mask = train_mask

    # 创建图数据
    data = Data(x=x, y=y, edge_index=edge_index, edge_attr=edge_attr, edge_labels=edge_labels,
                train_mask=train_mask, val_mask=val_mask, test_mask=test_mask,
                ho_index=ho_index, ho_num=ho_index.shape[1])

    return data


def generate_transcitations_graph(data_name):
    """generate transcitations graph"""

    graph = generate_transcitations_C2graph(data_name)
    paper_num = graph.node_num()
    type_num = graph.node_label_num()
    citation_num = graph.edge_num()

    # node features
    node_features = [[0 for _ in range(type_num)] for _ in range(paper_num)]
    for x_citation in graph.get_node_ids():
        x_type = graph.get_node_label_by_nid(x_citation)
        node_features[x_citation][x_type] = 1
    x = torch.tensor(node_features, dtype=torch.float)

    # node labels
    y_labels = [[0 for _ in range(type_num)] for _ in range(paper_num)]
    for _y in graph.get_node_ids():
        y_type = graph.get_node_label_by_nid(_y)
        y_labels[_y][y_type] = 1
    y = torch.tensor(y_labels, dtype=torch.float)

    # edge index
    edge_i = []
    edge_j = []
    for edge_id in range(graph.edge_num()):
        (i, j) = graph.get_edge_by_id(edge_id)
        edge_i.append(i)
        edge_j.append(j)
    edge_index = torch.tensor([edge_i, edge_j], dtype=torch.long)

    # edge features
    edge_features = [[0, 0] for _ in range(citation_num)]
    for edge_id in range(graph.edge_num()):
        e_labels = graph.get_edge_label_by_id(edge_id, single=False)
        if 0 in e_labels:
            edge_features[edge_id][0] = 1

    edge_attr = torch.tensor(edge_features, dtype=torch.float)

    # edge labels
    edge_y = [[0, 0] for _ in range(citation_num)]
    for edge_id in range(graph.edge_num()):
        e_labels = graph.get_edge_label_by_id(edge_id, single=False)
        if 0 in e_labels:
            edge_y[edge_id][0] = 1
        if 1 in e_labels:
            edge_y[edge_id][1] = 1
    edge_labels = torch.tensor(edge_y, dtype=torch.float)

    # ho index
    ho_index = graph.compute_ho_index()
    ho_index = torch.tensor(ho_index, dtype=torch.long)

    # train, val, test mask
    train_mask = torch.tensor([True for _ in range(paper_num)], dtype=torch.bool)
    val_mask = train_mask
    test_mask = train_mask

    # 创建图数据
    data = Data(x=x, y=y, edge_index=edge_index, edge_attr=edge_attr, edge_labels=edge_labels,
                train_mask=train_mask, val_mask=val_mask, test_mask=test_mask,
                ho_index=ho_index, ho_num=ho_index.shape[1])

    return data


# ---------------------------
# generate PyG graphs
# ---------------------------


def generate_datasets(data_name, graph_type):
    if graph_type == "commontype":
        generate_handle = generate_commontype_graph
    else:
        generate_handle = generate_transcitations_graph
    train_dataset = []
    valid_dataset = []

    print(f"generating {data_name} | {graph_type}")

    total = 200
    count = 0
    for _ in range(200):
        train_dataset.append(generate_handle(data_name))
        count += 1
        process_bar.process_bar(count, total)

    total = 50
    count = 0
    for _ in range(50):
        valid_dataset.append(generate_handle(data_name))
        count += 1
        process_bar.process_bar(count, total)

    pickle_util.dump_object(train_dataset, r"../../datasets/planetoid/processed/train_%s_%s.plk" % (graph_type, data_name))
    pickle_util.dump_object(valid_dataset, "../../datasets/planetoid/processed/valid_%s_%s.plk" % (graph_type, data_name))


def read_datasets(data_name, graph_type):
    train_dataset = pickle_util.load_object(r"../../datasets/planetoid/processed/train_%s_%s.plk" % (graph_type, data_name))
    valid_dataset = pickle_util.load_object(r"../../datasets/planetoid/processed/valid_%s_%s.plk" % (graph_type, data_name))
    return train_dataset, valid_dataset


if __name__ == '__main__':
    # for dname in ["cora", "pub", "citeseer"]:
    #     for dtype in ["commontype", "transcitations"]:
    #         generate_datasets(dname, dtype)

    # -- test
    for dname in ["cora", "pub", "citeseer"]:
        commontype_data = generate_commontype_C2graph(dname)
        transcites_data = generate_transcitations_C2graph(dname)
        print(dname)


