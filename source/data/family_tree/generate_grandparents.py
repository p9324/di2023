# encoding=utf-8
import random
import torch
from torch_geometric.data import Data
from data.family_tree import family
from models.utils import compute_ho_index
from zzqlib.io import pickle_util


def pair2edge(i, j, n):
    return i*n+j


def edge2pair(e, n):
    i = int(e / n)
    j = int(e % n)


def generate_data(is_sample=False):
    """generate family tree and filter it by parents"""

    if is_sample:
        n_person = 11
        family_tree = family.sample_generate_family()
    else:
        n_person = random.randint(8, 18)
        family_tree = family.randomly_generate_family(n_person)

    # nodes
    node_features = [[1] for _ in range(family_tree.n_people)]
    x = torch.tensor(node_features, dtype=torch.float)

    # make edge features and initialized by original relations
    edge_features = [None for _ in range(n_person*n_person)]
    complete_relations = family_tree._complete_relations

    # has_original_relations = family_tree.has_original_relations()
    edge_i_list = []
    edge_j_list = []
    for i in range(n_person):
        for j in range(n_person):
            # if has_original_relations[i, j] == 1:
            #     edge_i_list.append(i)
            #     edge_j_list.append(j)

            edge_i_list.append(i)
            edge_j_list.append(j)

            feature_i_j = [0, 0]  # [has_parent, has_grandparent]
            check_relations = [family.RELATION_MAPPING["has_parent"]]
            for k in range(1):
                idx = check_relations[k]
                if complete_relations[i, j, idx] == 1:
                    feature_i_j[k] = 1

            edge_features[pair2edge(i, j, n_person)] = feature_i_j

    # edge index : fully connected
    edge_index = torch.tensor([edge_i_list, edge_j_list], dtype=torch.long)

    # edge attr : only original relations
    edge_attr = torch.tensor(edge_features, dtype=torch.float)

    # ho index
    ho_index = compute_ho_index(edge_index)

    # train, val, test mask
    train_mask = torch.tensor([True for _ in range(n_person)], dtype=torch.bool)
    val_mask = train_mask
    test_mask = train_mask

    # node labels
    y = torch.tensor([1 for _ in range(n_person)])

    # final edge labels
    edge_final_features = [None for _ in range(n_person * n_person)]

    for i in range(n_person):
        for j in range(n_person):
            feature_i_j = [0, 0]  # [has_parent, has_grandparent]
            check_relations = [family.RELATION_MAPPING["has_parent"],
                               family.RELATION_MAPPING["has_grandparent"]]
            for k in range(2):
                idx = check_relations[k]
                if complete_relations[i, j, idx] == 1:
                    feature_i_j[k] = 1
            edge_final_features[pair2edge(i, j, n_person)] = feature_i_j

    edge_labels = torch.tensor(edge_final_features, dtype=torch.float)

    # 创建图数据
    data = Data(x=x, y=y, edge_index=edge_index, edge_attr=edge_attr, edge_labels=edge_labels,
                train_mask=train_mask, val_mask=val_mask, test_mask=test_mask,
                ho_index=ho_index, ho_num=ho_index.shape[1])

    return data


def generate_datasets():
    train_dataset = [generate_data() for _ in range(200)]
    valid_dataset = [generate_data() for _ in range(50)]
    pickle_util.dump_object(train_dataset, "../../datasets/family_tree/train_grandparents.plk")
    pickle_util.dump_object(valid_dataset, "../../datasets/family_tree/valid_grandparents.plk")


def read_datasets():
    train_dataset = pickle_util.load_object("../../datasets/family_tree/train_grandparents.plk")
    valid_dataset = pickle_util.load_object("../../datasets/family_tree/valid_grandparents.plk")
    return train_dataset, valid_dataset


if __name__ == '__main__':
    # planetoid = generate_data(is_sample=True)
    # print(os.path.abspath("../../datasets/family_tree"))
    generate_datasets()
    # train_data, valid_data = read_datasets()
    print("ok")
