# encoding = utf-8
import numpy as np
from numpy import random
import random as rand

RELATION_MAPPING = {
    "has_husband": 0,
    "has_wife": 1,
    "has_father": 2,
    "has_mother": 3,
    "has_son": 4,
    "has_daughter": 5,
    "has_sister": 6,
    "has_uncle": 7,
    "has_grandparent": 8,
    "has_maternal_great_uncle": 9,
    "has_parent": 10
}


class Family(object):
    """Family tree class to support queries about relations between family members.

    complete relations: a matrix of shape [n_people, n_people, 11]
        they are in the order: has_husband, has_wife, has_father, has_mother, has_son, has_daughter,
                               has_sister, has_uncle, has_grandparent, has_maternal_great_uncle,
                               has_parent

    Args:
      n_people: The number of people in the family tree.
      relations: The relations between family members. The relations should be an
          matrix of shape [n_people, n_people, 6]. The relations in the order
          are: husband, wife, father, mother, son, daughter.


    """

    def __init__(self, n_people, relations):
        self._n = n_people
        self._original_relations = relations
        self._identity = np.identity(self._n, dtype='float')
        self._complete_relations = np.zeros((self._n, self._n, 11), dtype='float')
        self.complete_relations()

    def mul(self, x, y):
        # the reason why self._identity should be reduced is
        # that all relations are not self-reference
        return np.clip(np.matmul(x, y) - self._identity, 0, 1)

    @property
    def n_people(self):
        return self._n

    @property
    def husband(self):
        return self._original_relations[:, :, 0]

    @property
    def wife(self):
        return self._original_relations[:, :, 1]

    @property
    def father(self):
        return self._original_relations[:, :, 2]

    @property
    def mother(self):
        return self._original_relations[:, :, 3]

    @property
    def son(self):
        return self._original_relations[:, :, 4]

    @property
    def daughter(self):
        return self._original_relations[:, :, 5]

    @property
    def original_relations(self):
        return self._original_relations

    def has_father(self):
        return self.father

    def has_daughter(self):
        return self.daughter

    def has_sister(self):
        # daughter_cnt = self.daughter.sum(axis=1)  # who has daughters
        # is_daughter = np.clip(self.daughter.sum(axis=0), 0, 1)
        # return ((np.matmul(self.father, daughter_cnt) - is_daughter) >
        #         0).astype('float')
        # The wrong implementation: count herself as sister.
        # return self.mul(self.father, self.daughter).max(axis=1)
        return self.mul(self.father, self.daughter)

    def has_parents(self):
        return np.clip(self.father + self.mother, 0, 1)

    def has_grandfather(self):
        return self.mul(self.has_parents(), self.father)

    def has_grandmother(self):
        return self.mul(self.has_parents(), self.mother)

    def has_grandparents(self):
        parents = self.has_parents()
        return self.mul(parents, parents)

    def has_uncle(self):
        return np.clip(self.mul(self.has_grandparents(), self.son) - self.father, 0, 1)
        # The wrong Implementation: not exclude father.
        # return self.mul(self.get_grandparents(), self.son)

    def has_maternal_great_uncle(self):
        return self.mul(self.mul(self.has_grandmother(), self.mother), self.son)

    def complete_relations(self):
        # copy original relations
        self._complete_relations[:, :, 0:6] = self._original_relations

        # fill added relations
        new_rels = [
            self.has_sister(),
            self.has_uncle(),
            self.has_grandparents(),
            self.has_maternal_great_uncle(),
            self.has_parents()
        ]
        for i in range(6, 11):
            self._complete_relations[:, :, i] = new_rels[i-6]

        return self._complete_relations

    def has_original_relations(self):
        return np.clip(self._original_relations.sum(axis=2), 0, 1)


def randomly_generate_family(n, p_marriage=0.8, verbose=False):
    """Randomly generate family trees.

    Mimic the process of families growing using a timeline. Each time a new person
    is created, randomly sample the gender and parents (could be none, indicating
    not included in the family tree) of the person. Also maintain lists of singles
    of each gender. With probability $p_marrige, randomly pick two from each list
    to be married. Finally randomly permute the order of people.

    Args:
      n: The number of people in the family tree.
      p_marriage: The probability of marriage happens each time.
      verbose: print the marriage and child born process if verbose=True.
    Returns:
      A family tree instance of $n people.
    """
    assert n > 0
    ids = list(random.permutation(n))

    single_m = []
    single_w = []
    couples = [None]
    # The relations are: husband, wife, father, mother, son, daughter
    rel = np.zeros((n, n, 6))
    fathers = [None for i in range(n)]
    mothers = [None for i in range(n)]

    def add_couple(man, woman):
        """Add a couple relation among (man, woman)."""
        couples.append((man, woman))
        rel[woman, man, 0] = 1  # husband
        rel[man, woman, 1] = 1  # wife
        if verbose:
            print('couple', man, woman)

    def add_child(parents, child, gender):
        """Add a child relation between parents and the child according to gender."""
        father, mother = parents
        fathers[child] = father
        mothers[child] = mother
        rel[child, father, 2] = 1  # father
        rel[child, mother, 3] = 1  # mother
        if gender == 0:  # son
            rel[father, child, 4] = 1
            rel[mother, child, 4] = 1
        else:  # daughter
            rel[father, child, 5] = 1
            rel[mother, child, 5] = 1
        if verbose:
            print('child', father, mother, child, gender)

    def check_relations(man, woman):
        """Disable marriage between cousins."""
        if fathers[man] is None or fathers[woman] is None:
            return True
        if fathers[man] == fathers[woman]:
            return False

        def same_parent(x, y):
            return fathers[x] is not None and fathers[y] is not None and fathers[
                x] == fathers[y]

        for x in [fathers[man], mothers[man]]:
            for y in [fathers[woman], mothers[woman]]:
                if same_parent(man, y) or same_parent(woman, x) or same_parent(x, y):
                    return False
        return True

    while ids:
        x = ids.pop()
        gender = random.randint(2)
        parents = rand.choice(couples)
        if gender == 0:
            single_m.append(x)
        else:
            single_w.append(x)
        if parents is not None:
            add_child(parents, x, gender)

        if random.rand() < p_marriage and len(single_m) > 0 and len(single_w) > 0:
            mi = random.randint(len(single_m))
            wi = random.randint(len(single_w))
            man = single_m[mi]
            woman = single_w[wi]
            if check_relations(man, woman):
                add_couple(man, woman)
                del single_m[mi]
                del single_w[wi]

    if verbose:
        print("generation done")
        # print(rel)
    return Family(n, rel)


def sample_generate_family():
    # 0 has_husband, 1 has_wife, 2 has_father, 3 has_mother, 4 has_son, 5 has_daughter

    def add_couple(man, woman):
        """Add a couple relation among (man, woman)."""
        rel[woman, man, 0] = 1  # husband
        rel[man, woman, 1] = 1  # wife

    def add_child(parents, child, gender):
        """Add a child relation between parents and the child according to gender."""
        father, mother = parents
        rel[child, father, 2] = 1  # father
        rel[child, mother, 3] = 1  # mother
        if gender == 0:  # son
            rel[father, child, 4] = 1
            rel[mother, child, 4] = 1
        else:  # daughter
            rel[father, child, 5] = 1
            rel[mother, child, 5] = 1

    n = 11
    rel = np.zeros((n, n, 6))

    # (0,1) (5,6),(3,7),(4,8) are couples
    add_couple(0, 1)
    add_couple(5, 6)
    add_couple(7, 3)
    add_couple(8, 4)

    # (0,1)'s children
    add_child((0, 1), 2, 0)
    add_child((0, 1), 3, 1)
    add_child((0, 1), 4, 1)

    # (5,6)'s children
    add_child((5, 6), 7, 0)
    add_child((5, 6), 8, 0)

    # (7,3)'s child
    add_child((7, 3), 9, 0)

    # (8,4)'s child
    add_child((8, 4), 10, 1)

    return Family(n, rel)


if __name__ == '__main__':
    # family_data = randomly_generate_family(10, verbose=True)
    family_data = sample_generate_family()
    family_data.has_sister()
    print("done")

