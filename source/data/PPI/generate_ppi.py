import torch
from models.utils import compute_ho_index
from torch_geometric.data import Data
from torch_geometric.datasets import PPI


root = r"../../datasets/PPI/raw"


def generate_ppi_dataset(dataset):
    """generate graphs"""

    data_list = []
    for i in range(dataset.len()):
        graph = dataset[i]
        # ho index
        # ho_index = compute_ho_index(edge_index=dataset.data.edge_index)
        ho_index = torch.tensor([[], []], dtype=torch.long)

        edge_num = graph.num_edges
        edge_features = [[1] for _ in range(edge_num)]
        edge_attr = torch.tensor(edge_features, dtype=torch.float)
        edge_labels = torch.tensor(edge_features, dtype=torch.float)

        # 创建图数据
        data = Data(
            x=graph.x,
            y=graph.y,
            edge_index=graph.edge_index,
            edge_attr=edge_attr,
            edge_labels=edge_labels,
            ho_index=ho_index, ho_num=ho_index.shape[1])
        data_list.append(data)

    return data_list


def generate_train_val_datasets():
    train_dataset = PPI(root, split='train')
    val_dataset = PPI(root, split='val')
    test_dataset = PPI(root, split='test')
    return generate_ppi_dataset(train_dataset), generate_ppi_dataset(val_dataset)


if __name__ == '__main__':
    train_data, val_data = generate_train_val_datasets()
