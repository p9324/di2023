import torch
from training.model_trainer import ModelTrainer
from models.npgnn.npr_gnn_model import NPRGNNModel


parameters = {

    "task_name": "planetoid_%s_%s",
    "task_type": "multiclass",   # multiclass or binary
    "output_type": "node",       # node or edge

    "training_path": r"../datasets/planetoid/processed/train_%s_%s.plk",
    "valid_path": r"../datasets/planetoid/processed/valid_%s_%s.plk",

    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,  # 为了防止过拟合
    "device": 'cpu',
    "criterion": torch.nn.CrossEntropyLoss,

    "node_in_channels": 7,
    "node_out_channels": 7,
    "edge_in_channels": 1,
    "edge_out_channels": 1,

    "binary_pred_index": -1,

    "log_dir": r"log/planetoid",

}


def get_parameters(data_name, data_type):
    par = parameters.copy()
    par["task_name"] = par["task_name"] % (data_name, data_type)
    par["training_path"] = par["training_path"] % (data_type, data_name)
    par["valid_path"] = par["valid_path"] % (data_type, data_name)
    if data_type == "transcitations":
        par["output_type"] = "edge"
        par["edge_in_channels"] = 2
        par["edge_out_channels"] = 2
    if data_name == "cora":
        par["node_in_channels"] = 7
        par["node_out_channels"] = 7
    if data_name == "citeseer":
        par["node_in_channels"] = 6
        par["node_out_channels"] = 6
    if data_name == "pub":
        par["node_in_channels"] = 3
        par["node_out_channels"] = 3
    return par


if __name__ == '__main__':

    # conf = get_parameters("cora", "transcitations")
    #
    # model = MPGNNModel(
    #     conf["node_in_channels"],
    #     conf["node_out_channels"],
    #     conf["edge_in_channels"],
    #     conf["edge_out_channels"],
    # )
    #
    # trainer = ModelTrainer()
    # trainer.initialize(model, conf)
    # trainer.train_epochs()
    # trainer.dump_logs()

    for dname in ["cora", "citeseer", "pub"]:
        for dtype in ["commontype", "transcitations"]:

            print(f"learning {dtype} from {dname}")
            conf = get_parameters(dname, dtype)

            model = NPRGNNModel(
                conf["node_in_channels"],
                conf["node_out_channels"],
                conf["edge_in_channels"],
                conf["edge_out_channels"],
                node_aggr='mean',
                edge_aggr='mean'
            )

            trainer = ModelTrainer()
            trainer.initialize(model, conf)
            trainer.train_epochs()
            trainer.dump_logs()





"""

parameters = {

    "task_name": "planetoid_cora_commontype",
    "task_type": "multiclass",   # multiclass or binary
    "output_type": "node",       # node or edge

    "training_path": r"../datasets/planetoid/processed/train_commontype_cora.plk",
    "valid_path": r"../datasets/planetoid/processed/valid_commontype_cora.plk",

    "epochs": 200,
    "batch_size": 32,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,  # 为了防止过拟合
    "device": 'cpu',
    "criterion": torch.nn.CrossEntropyLoss,

    "node_in_channels": 7,
    "node_out_channels": 7,
    "edge_in_channels": 1,
    "edge_out_channels": 1,

    "binary_pred_index": -1,

    "log_dir": r"log/planetoid",

}
"""