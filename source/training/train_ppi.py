import torch
from training.model_trainer import ModelTrainer
from models.npgnn.npr_gnn_model import NPRGNNModel
from models.gin_model import GINNodeModel
from models.gnn.acr_gnn import ACRGNodeModel
from data.PPI.generate_ppi import generate_train_val_datasets



parameters = {

    "task_name": "PPI classification",
    "task_type": "multiclass",   # multiclass or binary
    "output_type": "node",       # node or edge

    "training_data": None,
    "valid_data": None,

    "epochs": 500,
    "batch_size": 2,
    "learning_rate": 0.01,
    "weight_decay": 5e-4,  # 为了防止过拟合
    "device": 'cpu',
    "criterion": torch.nn.CrossEntropyLoss,

    "node_in_channels": 50,
    "node_out_channels": 121,
    "edge_in_channels": 1,
    "edge_out_channels": 1,

    "binary_pred_index": -1,
    "log_dir": r"log/PPI",

}


def get_parameters():
    train_dataset, val_dataset = generate_train_val_datasets()
    par = parameters.copy()
    par["training_data"] = train_dataset
    par["valid_data"] = val_dataset
    return par


def train_gin():
    par = get_parameters()
    model = GINNodeModel(
        par["node_in_channels"],
        par["node_out_channels"],
    )
    trainer = ModelTrainer()
    trainer.initialize(model, par)
    trainer.train_epochs()
    trainer.dump_logs()


def train_acrgnn():
    par = get_parameters()
    model = ACRGNodeModel(
        par["node_in_channels"],
        par["node_out_channels"],
    )
    trainer = ModelTrainer()
    trainer.initialize(model, par)
    trainer.train_epochs()
    trainer.dump_logs()


def train_mpgnn():
    conf = get_parameters()
    model = NPRGNNModel(
        conf["node_in_channels"],
        conf["node_out_channels"],
        conf["edge_in_channels"],
        conf["edge_out_channels"],
        node_aggr='sum',
        edge_aggr='sum'
    )

    trainer = ModelTrainer()
    trainer.initialize(model, conf)
    trainer.train_epochs()
    trainer.dump_logs()




if __name__ == '__main__':

    # train_gin()
    train_acrgnn()

